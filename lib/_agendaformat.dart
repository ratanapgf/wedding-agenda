import 'package:flutter/material.dart';

class AgendaForm extends StatelessWidget {
  final String time;
  final String todo;

  AgendaForm({required this.time, required this.todo});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 90.0,
      height: 60.0,
      margin: const EdgeInsets.symmetric(vertical: 6.0),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(30.0),
        border: Border.all(color: Colors.white),
      ),
      child: Row(
        children: [
          Container(
            width: 100.0,
            height: 100.0,
            decoration: BoxDecoration(
              color: Colors.green,
              border: Border.all(color: Colors.white),
              borderRadius: BorderRadius.circular(30.0),
            ),
            child:  Center(
              child: Text(
                time,
                style: const TextStyle(fontSize: 30.0),
              ),
            ),
          ),
          const SizedBox(width: 5.0),
           Expanded(
            child: Text(
              todo,
              style: const TextStyle(fontSize: 20.0),
            ),
          ),
          Radio(value: '', groupValue: '', onChanged: (value) {}),
        ],
      ),
    );
  }
}
