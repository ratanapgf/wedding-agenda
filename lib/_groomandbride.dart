import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class GroomandBride extends StatelessWidget {
  const GroomandBride({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/Married.jpg'),
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              DateFormat('dd MMMM yyyy').format(DateTime.now()),
              style: const TextStyle(
                fontSize: 25.0,
                color: Colors.white,
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Column(
                  children: const [
                    CircleAvatar(
                      radius: 75.0,
                      backgroundColor: Colors.black,
                      child: CircleAvatar(
                        radius: 70.0,
                        backgroundImage: AssetImage('assets/Groom.png'),
                      ),
                    ),
                    SizedBox(height: 10.0),
                    Text(
                      'Groom',
                      style: TextStyle(fontSize: 20.0, color: Colors.white),
                    ),
                  ],
                ),
                const Icon(
                  Icons.favorite,
                  color: Colors.red,
                  size: 75.0,
                ),
                Column(
                  children: const [
                    CircleAvatar(
                      radius: 75.0,
                      backgroundColor: Colors.black,
                      child: CircleAvatar(
                        radius: 70.0,
                        backgroundImage: AssetImage('assets/Bride.png'),
                      ),
                    ),
                    SizedBox(height: 10.0),
                    Text(
                      'Bride',
                      style: TextStyle(fontSize: 20.0, color: Colors.white),
                    ),
                  ],
                ),
              ],
            ),
            Text(
              DateFormat('hh:mm:ss').format(DateTime.now()),
              style: const TextStyle(
                fontSize: 25.0,
                color: Colors.white,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
