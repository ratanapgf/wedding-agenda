import 'package:flutter/material.dart';
import '_groomandbride.dart';
import '_agendaformat.dart';
import 'agendalist.dart';

class MyApp extends StatelessWidget {
  MyApp({Key? key}) : super(key: key);
  final agenda = AgendaList();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.green,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const GroomandBride(),
            Padding(
              padding: const EdgeInsets.only(
                top: 20,
                left: 25,
              ),
              child: Container(
                width: 90.0,
                height: 60.0,
                decoration: BoxDecoration(
                  color: Colors.green,
                  border: Border.all(color: Colors.white),
                  borderRadius: BorderRadius.circular(30.0),
                ),
                child: const Center(
                  child: Text(
                    'Day 1',
                    style: TextStyle(fontSize: 30.0),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                left: 20,
                right: 20,
              ),
              child: ListView.builder(
                shrinkWrap: true,
                primary: false,
                itemCount: agenda.agendalist.length,
                itemBuilder: (context, index) => AgendaForm(
                  time: agenda.agendalist[index].time,
                  todo: agenda.agendalist[index].todo,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                left: 25,
              ),
              child: Container(
                width: 90.0,
                height: 60.0,
                decoration: BoxDecoration(
                  color: Colors.green,
                  border: Border.all(color: Colors.white),
                  borderRadius: BorderRadius.circular(30.0),
                ),
                child: const Center(
                  child: Text(
                    'End',
                    style: TextStyle(fontSize: 30.0),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
