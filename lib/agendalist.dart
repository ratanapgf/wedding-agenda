import 'agendaModule.dart';

class AgendaList {
  List<AgenModule> agendalist = [
    AgenModule(
      time: '5:30',
      todo:'សំពះពេលាជ័យ',
    ),
    AgenModule(
     time: '7:00',
     todo: 'ហែរជំនូន',
    ),
    AgenModule(
       time:'7:30',
     todo: 'ពិសាអាហារពេលព្រឹក',
    ),
    AgenModule(
     time: '9:00',
     todo: 'កាត់សក់បង្កក់សិរី',
    ),
    AgenModule(
      time:'9:40',
      todo:'របាំជូនពរ',
    ),
    AgenModule(
      time:'10:00',
     todo: 'សែនដូនតា',
    ),
    AgenModule(
     time: '10:30',
      todo:'សំពះផ្ចឹម បង្វិលពពិល គូចចំណងដៃ',
    ),
    AgenModule(
      time:'11:00',
     todo: 'បាចផ្កាស្លា & ព្រះថោងនាងនាគ',
    ),
    AgenModule(
      time:'12:00',
      todo:'ពិសាអាហារថ្ងៃត្រង់',
    ),
    AgenModule(
      time:'13:30',
     todo:'សែនក្រុពាលី',
    ),
    AgenModule(
    time: '15:30',
      todo:'សូត្រមន្ត',
    ),
    AgenModule(
      time:'16:30',
      todo:'ជុំពេលា & កាត់ខាន់ស្លា',
    ),
    AgenModule(
      time:'17:00',
      todo:'ពិសាអាហារពេលល្ងាច',
    ),
  ];
}
